// Bài 1: Tính Ngày Tháng Năm
function tinhNgayThangNam() {
  var dateValue = document.getElementById("ngayThangNam").value;
  var today = new Date(dateValue);

  var resultEx1 = document.getElementById("result-ex-1");

  var ngayHomQua = today.setDate(today.getDate() - 1);
  var ngayMai = today.setDate(today.getDate() + 2);

  resultEx1.innerHTML = `<p>Ngày hôm qua: ${new Date(ngayHomQua)}</p>
                           <p>Ngày mai: ${new Date(ngayMai)}</p>`;
}

// Bài 2: Tính Ngày

function tinhNgay() {
  var monthValue = document.getElementById("monthValue").value * 1;
  var yearValue = document.getElementById("yearValue").value * 1;

  var soNgay;

  switch (monthValue) {
    case 1:
    case 3:
    case 5:
    case 7:
    case 8:
    case 10:
    case 12:
      soNgay = "31 ngày";
      break;
    case 4:
    case 6:
    case 9:
    case 11:
      soNgay = "30 Ngày";
      break;
  }

  if (monthValue == 2) {
    if (yearValue % 4 == 0 && yearValue % 100 != 0) {
      soNgay = "29 ngày";
    } else {
      soNgay = "28 ngày";
    }
  }
  if (monthValue == 2 && yearValue == 0) {
    soNgay = "29 ngày";
  }

  document.getElementById(
    "result-ex-2"
  ).innerHTML = ` Số ngày trong tháng là: ${soNgay} `;
}

// Bài 3: phát âm ba chữ số

function phatAmBaChuSo() {
  var number = document.getElementById("txt-number").value * 1;
  var phienAm = [
    "không",
    "một",
    "hai",
    "ba",
    "bốn",
    "năm",
    "sáu",
    "bảy",
    "tám",
    "chín",
  ];
  var soDonVi = number % 10;
  var soHangChuc = Math.floor(number / 10) % 10;
  var soHangTram = Math.floor(number / 100);

  var resultEx3 = document.getElementById("result-ex-3");
  var phatAm;

  if (number >= 100 && number <= 999) {
    if (soDonVi != 0 && soHangChuc != 0) {
      phatAm =
        phienAm[soHangTram] +
        " trăm" +
        " " +
        phienAm[soHangChuc] +
        " mươi" +
        " " +
        phienAm[soDonVi];
    }
    if (soDonVi == 0 && soHangChuc == 0) {
      phatAm = phienAm[soHangTram] + " trăm";
    }
    if (soDonVi == 0 && soHangChuc != 0) {
      phatAm =
        phienAm[soHangTram] + " trăm" + " " + phienAm[soHangChuc] + " mươi";
    }
    if (soDonVi != 0 && soHangChuc == 0) {
      phatAm = phienAm[soHangTram] + " trăm" + " linh" + " " + phienAm[soDonVi];
    }
  } else {
    alert("Số không hợp lệ! Hãy nhập số có ba chữ số!");
  }

  resultEx3.innerHTML = `${phatAm}`;
}

// Bài tập 4: Tính quãng đường xa nhất

function tinhQuangDuong() {
  var tenSV1 = document.getElementById("tenSinhVien1").value;
  var toaDoXSV1 = document.getElementById("toaDoXSinhVien1").value * 1;
  var toaDoYSV1 = document.getElementById("toaDoYSinhVien1").value * 1;

  var tenSV2 = document.getElementById("tenSinhVien2").value;
  var toaDoXSV2 = document.getElementById("toaDoXSinhVien2").value * 1;
  var toaDoYSV2 = document.getElementById("toaDoYSinhVien2").value * 1;

  var tenSV3 = document.getElementById("tenSinhVien3").value;
  var toaDoXSV3 = document.getElementById("toaDoXSinhVien3").value * 1;
  var toaDoYSV3 = document.getElementById("toaDoYSinhVien3").value * 1;

  var toaDoXTruongHoc = document.getElementById("toaDoXTruongHoc").value * 1;
  var toaDoYTruongHoc = document.getElementById("toaDoYTruongHoc").value * 1;

  var khoangCachSV1 = Math.sqrt(
    Math.pow(toaDoXSV1 - toaDoXTruongHoc, 2) +
      Math.pow(toaDoYSV1 - toaDoYTruongHoc, 2)
  );

  var khoangCachSV2 = Math.sqrt(
    Math.pow(toaDoXSV2 - toaDoXTruongHoc, 2) +
      Math.pow(toaDoYSV2 - toaDoYTruongHoc, 2)
  );

  var khoangCachSV3 = Math.sqrt(
    Math.pow(toaDoXSV3 - toaDoXTruongHoc, 2) +
      Math.pow(toaDoYSV3 - toaDoYTruongHoc, 2)
  );

  var thongTinSinhVien = [
    {
      ten: tenSV1,
      khoangCach: khoangCachSV1,
    },
    {
      ten: tenSV2,
      khoangCach: khoangCachSV2,
    },
    {
      ten: tenSV3,
      khoangCach: khoangCachSV3,
    },
  ];

  var maxKhoangCach = thongTinSinhVien[0].khoangCach;
  for (var index = 0; index < thongTinSinhVien.length; index++) {
    if (maxKhoangCach < thongTinSinhVien[index].khoangCach) {
      maxKhoangCach = thongTinSinhVien[index].khoangCach;
    }
  }

  var tenSvCoKhoangCachDaiNhat;
  thongTinSinhVien.forEach(function (sv) {
    if (maxKhoangCach == sv.khoangCach) {
      tenSvCoKhoangCachDaiNhat = sv.ten;
    }
  });

  document.getElementById(
    "result-ex-4"
  ).innerHTML = `Sinh viên cách trường xa nhất: ${tenSvCoKhoangCachDaiNhat}`;
}
